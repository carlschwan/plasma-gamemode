// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#pragma once

#include <QDBusArgument>
#include <QDBusMetaType>
#include <QDBusObjectPath>

struct GamesMapEntry {
    int pid = -1;
    QDBusObjectPath path;
};

QDBusArgument& operator <<(QDBusArgument& argument, const GamesMapEntry& arg);
const QDBusArgument& operator >>(const QDBusArgument& argument, GamesMapEntry& arg);
using ListGamesMap = QList<GamesMapEntry>;

// using ListGamesMap = QMap<int, QDBusObjectPath>;
Q_DECLARE_METATYPE(GamesMapEntry);
Q_DECLARE_METATYPE(ListGamesMap);

namespace Types
{
static void registerTypes()
{
    static bool registered = false;
    if (registered) {
        return;
    }
    registered = true;

    qDBusRegisterMetaType<GamesMapEntry>();
    qDBusRegisterMetaType<ListGamesMap>();
}
}
