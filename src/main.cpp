// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

#include <QtQml/QQmlExtensionPlugin>
#include <QtQml/qqml.h>

#include "gamesmodel.h"
#include "types.h"
class Plugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")
public:
     void registerTypes(const char *uri) override
    {
        qmlRegisterType<GamesModel>(uri, 0, 1, "GamesModel");
        Types::registerTypes();
    }
};

#include "main.moc"
