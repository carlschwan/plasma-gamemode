# SPDX-License-Identifier: BSD-3-Clause
# SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

add_definitions(-DTRANSLATION_DOMAIN=\"plasma_applet_org.kde.plasma.gamemode\")

set(generated_SRCS)

set_source_files_properties(com.feralinteractive.GameMode.xml
    PROPERTIES
        NO_NAMESPACE TRUE
        INCLUDE types.h)
set_source_files_properties(com.feralinteractive.GameMode.Game.xml PROPERTIES NO_NAMESPACE TRUE)
qt_add_dbus_interface(generated_SRCS org.freedesktop.DBus.Properties.xml org.freedesktop.DBus.Properties)
qt_add_dbus_interface(generated_SRCS com.feralinteractive.GameMode.Game.xml com.feralinteractive.GameMode.Game)
qt_add_dbus_interface(generated_SRCS com.feralinteractive.GameMode.xml com.feralinteractive.GameMode)

add_library(plasma-gamemode main.cpp gamesmodel.cpp types.cpp ${generated_SRCS})
target_link_libraries(plasma-gamemode
    Qt5::DBus
    Qt5::Quick
)

set(PRIVATE_QML_INSTALL_DIR ${QML_INSTALL_DIR}/org/kde/plasma/private/gamemode)
install(TARGETS plasma-gamemode DESTINATION ${PRIVATE_QML_INSTALL_DIR})
install(FILES qmldir DESTINATION ${PRIVATE_QML_INSTALL_DIR})

add_subdirectory(applet)
